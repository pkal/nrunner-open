**Nrunner - Neural Network Runner**

Program nrunner służy do przetwarzania danych wejściowych za pomocą sieci neuronowej. Sieć służy do przewidywania poziomu hałasu, powodowanego przez ruch samochodowy.

Nrunner wykorzystuje bibliotekę JPMML (https://github.com/jpmml). Sieć wczytywana jest z pliku network.xml i musi być w formacie PMML, w wersji czytelnej dla tej biblioteki.

**Sposób użycia**

Program uruchamiany jest skryptem w podkatalogu bin.

./nrunner [-s][-h] (Linux, systemy uniksowe)

lub:

nrunner.bat [-s][-h] (Windows)

Opcje:

-s Tryb cichy - nie wyświetlaj komunikatów

-h --help Wyświetl pomoc

**Licencja**

To jest wolne oprogramowanie. Można z niego korzystać zgodnie z licencją GNU Affero General Public w wersji 3.0 lub późniejszej.

**Autor**

Piotr Kalinowski (https://bitbucket.org/pkal/)
