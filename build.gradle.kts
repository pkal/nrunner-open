import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    application
}
group = "pkalinow.neural"
version = "1.0.1-SNAPSHOT"

repositories {
    mavenCentral()
}
dependencies {
    implementation("org.junit.jupiter:junit-jupiter:5.4.2")
    implementation("org.jpmml:pmml-evaluator:1.5.11")
    implementation("org.jpmml:pmml-evaluator-extension:1.5.11")
    // JAX-B dependencies for JDK 9+
    implementation("jakarta.xml.bind:jakarta.xml.bind-api:2.3.2")
    implementation("org.glassfish.jaxb:jaxb-runtime:2.3.2")
    testImplementation(kotlin("test-junit5"))
}
tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}
tasks.test {
    useJUnitPlatform()
    maxHeapSize = "1G"
}
application {
    mainClassName = "MainKt"
}
distributions {
    main {
        contents {
            from(
                "network.xml",
                "przyklad1.csv",
                "przyklad2.csv",
                "README.md"
            )
        }
    }
}