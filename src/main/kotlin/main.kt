import org.dmg.pmml.FieldName
import org.jpmml.evaluator.Evaluator
import org.jpmml.evaluator.FieldValue
import org.jpmml.evaluator.LoadingModelEvaluatorBuilder
import java.io.File
import java.lang.IllegalArgumentException
import kotlin.system.exitProcess

const val NETWORK_FILENAME = "network.xml"

fun main(args: Array<String>) {
    if (args.contains("-h") || args.contains("--help")) {
        printHelp()
        readLine()
        exitProcess(0)
    }

    val silent = args.contains("-s")

    var networkFile = File(NETWORK_FILENAME)

    if (!networkFile.exists()) {
        networkFile = File("../$NETWORK_FILENAME")
    }

    if (!networkFile.exists()) {
        println("Nie znaleziono pliku ${networkFile.name} ")
        readLine()
        exitProcess(1)
    }

    if (!silent) {
        println("Nrunner z biblioteką JPMML (https://github.com/jpmml)")
        println("Autor: Piotr Kalinowski")
        println("Podaj dane w formacie:")
        println("Lmax,Lmin,kat1,kat2a,kat2b,tn,pw,t,w,c,ROK,MIEJSCE,NAWIERZCHNIA")
    }
    val input = readLine()
    val inputVector = parseInput(input)
    println(inputVector)

    val inputMap = LinkedHashMap<String, String>()
    val names = listOf<String>(
        "Lmax",
        "Lmin",
        "kat1",
        "kat2a",
        "kat2b",
        "tn",
        "pw",
        "t",
        "w",
        "c",
        "ROK",
        "MIEJSCE",
        "NAWIERZCHNIA"
    )
    for ((i, value) in names.withIndex()) {
        if (i < inputVector.size) {
            inputMap[value] = inputVector[i]
        } else {
            println("Zbyt krótki wektor danych")
            break
        }
    }
    if (!silent) {
        println("Wejście: $inputMap")
    }

    // Building a model evaluator from a PMML file
    val evaluator: Evaluator = LoadingModelEvaluatorBuilder()
        .load(networkFile)
        .build()

    // Performing the self-check
    evaluator.verify()

    val inputFields = evaluator.inputFields
    val arguments: MutableMap<FieldName, FieldValue> = LinkedHashMap()

    for (inputField in inputFields) {
        val name = inputField.name
        val value = inputField.prepare(inputMap[name.value])
        arguments[name] = value
    }

    val result = evaluator.evaluate(arguments)

    if (!silent) {
        println("Wynik: $result")
        readLine()
    } else {
        println(if (result.isEmpty()) "" else result.values.first())
    }
}

fun printHelp() {
    println("Nrunner z biblioteką JPMML (https://github.com/jpmml)")
    println("Autor: Piotr Kalinowski")
    println("Sposób użycia: \n" +
            "./nrunner [-s][-h]\n" +
            "LUB:\n" +
            "nrunner.bat [-s][-h]\n" +
            "Opcje:\n" +
            "-s Tryb cichy - nie wyświetlaj komunikatów\n" +
            "-h --help Wyświetl pomoc")
}

fun parseInput(input: String?): List<String> {
    if (input == null) {
        throw IllegalArgumentException("Brak danych")
    }
    return input.split(",")
}
